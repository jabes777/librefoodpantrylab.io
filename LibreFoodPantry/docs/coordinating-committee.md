---
id: coordinating-committee
title: Coordinating Committee
sidebar_label: Coordinating Committee
---

LibreFoodPantry is guided by a Coordinating Committee comprised of Trustees and Shop Managers. This body typically meets weekly.

## Agendas and Minutes

Before 2020-02-07, meetings were organized using Google Docs. Since 2020-02-07, the are now organized using an issue board on GitLab.

- [Shop-CC Meeting Issue Board](https://gitlab.com/groups/LibreFoodPantry/-/boards/1468199?&label_name[]=Shop-CC) (since 2020-02-07)
- [Google Drive folder](https://drive.google.com/drive/folders/12cbsPfniz38ZsOOz0NcaChl4W_Apn_-s) (prior to 2020-02-07)

## Trustees

- Darci Burdge, Nassau Community College, since 2019
- Heidi Ellis, Western New England University, since 2019
- Greg Hislop, Drexel University, since 2019
- Stoney Jackson, Western New England University, since 2019
- Lori Postner, Nassau Community College, since 2019
- Karl Wurst, Worcester State University, since 2019

## Shop Managers (Current and Past)

Shop managers manage a shop of developers that contribute to one or more LFP projects.
Shop managers and shop developers receive elevated privileges on the projects their shops contribute to.

- Darci Burdge, Nassau Community College, since 2019
- Heidi Ellis, Western New England University, since 2019
- Stoney Jackson, Western New England University, since 2019
- Lori Postner, Nassau Community College, since 2019
- Robert Walz, Western New England University, since 2019
- Karl Wurst, Worcester State University, since 2019
